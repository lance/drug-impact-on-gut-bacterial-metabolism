# -*- coding: utf-8 -*-
"""
Created on Fri Nov  8 13:59:19 2019

@author: clanc
"""
#-------------------------------------------------------------
# Functions for metabolic characterization of gut bacteria
#-------------------------------------------------------------



#libraries
import pandas as pd
import numpy as np
from scipy import stats
from statsmodels.stats.multitest import multipletests



def get_metabo_change(data_table, col_ids_g1, col_ids_g2, non_val_cols = 4,\
                      min_rep_val_mean = 2000, min_rep_val_sd = 750,\
                      mass_id = 'mass', rt_id = 'ret_time', \
                          cs_id = 'comp_spectra', equal_var = True, \
                              min_detected_peaks = 3, remove_outs = False):
    
    

    #without mass, rt, comp spec:
    data_table_numbs = data_table.iloc[:,0:data_table.shape[1]-3]
    
    # Replace nans with min_rep_value drawn from normal distribution
    missing = data_table_numbs.isnull()
    min_rep_vals = np.random.normal(min_rep_val_mean,min_rep_val_sd,sum(missing.sum()))
    
    for k in range(len(min_rep_vals)):
        if min_rep_vals[k] < 100:
            min_rep_vals[k] = np.random.normal(min_rep_val_mean,120,1)
            
    for k in range(len(min_rep_vals)):
        if min_rep_vals[k] > 3900:
            min_rep_vals[k] = np.random.normal(min_rep_val_mean,120,1)
    
    k = 0
    for m in range(data_table_numbs.shape[0]):
        for n in range(data_table_numbs.shape[1]):
            if np.isnan(data_table_numbs.iloc[m,n]):
                data_table_numbs.iloc[m,n] = min_rep_vals[k]
                k = k+1
    
    data_table.iloc[:,0:data_table.shape[1]-3] = data_table_numbs
    
    # initialize result table
    res_df_col_names = ['PerC', 'FC', 'P_val', 'mass', 'ret_time', 'comp_spectra']
    res_data_table = pd.DataFrame(columns = res_df_col_names)
    
    # loop through compounds and get changes of desiered groups
    for i in range(len(data_table.index)):

        metabo_id = data_table.index[i]
        
        T00_vals = data_table.loc[metabo_id,col_ids_g1]
        T12_vals = data_table.loc[metabo_id,col_ids_g2]
        
        
        res_row = []
        
        if sum(T00_vals > 3901)>=min_detected_peaks or \
            sum(T12_vals > 3901)>=min_detected_peaks:
            
            #remove outliers
            if remove_outs == True:
                #outlier detection and reduction function
                T00_mean = np.mean(T00_vals)
                T12_mean = np.mean(T12_vals)
                T00_sd = np.std(T00_vals)
                T12_sd = np.std(T12_vals)

                T00_vals = [val for val in T00_vals if abs(val-T00_mean)\
                                < 3*T00_sd]
                T12_vals = [val for val in T12_vals if abs(val-T12_mean)\
                                < 3*T12_sd]

            percent_change = ((np.mean(T12_vals)-np.mean(T00_vals))/np.mean(T00_vals))*100
            FC_val = np.mean(T12_vals)/np.mean(T00_vals)
            P_val = stats.ttest_ind(T12_vals, T00_vals, equal_var = equal_var, nan_policy = 'omit')[1]
            metabo_mass = data_table.loc[metabo_id, 'mass']
            metabo_ret_time = data_table.loc[metabo_id, 'ret_time']
            metabo_comp_spec = data_table.loc[metabo_id, 'comp_spectra']
            res_row.extend([percent_change, FC_val, P_val, metabo_mass, metabo_ret_time, metabo_comp_spec])
    
        if len(res_row) > 0:
            row_df = pd.DataFrame([res_row], index = [metabo_id], columns = res_df_col_names)
            res_data_table = res_data_table.append(row_df)
    
    
    # Add FDR adjusted (BH) P values
    
    P_vals = res_data_table['P_val']
    _,FDRvalues,_,_ = multipletests(P_vals, alpha=0.05, method='fdr_bh')
    
    res_data_table['FDR_vals'] = FDRvalues
    
    return(res_data_table)
    


def calculate_diff_matrix(comp_spectra_list):
    
    sim_matrix = pd.DataFrame(0, index = range(len(comp_spectra_list)), columns = range(len(comp_spectra_list)))
    
    for comp_id in range(len(comp_spectra_list)):
        #comp_id = 33 #testing
        for match_id in [i for i in range(len(comp_spectra_list))]:
            
            # test for matching ions
            #match_id = 5 #testing
            top3_comp = [x[0] for x in comp_spectra_list[comp_id]]
            top3_match = [x[0] for x in comp_spectra_list[match_id]]
            # reduce to top 3 intensities
            if len(top3_comp) > 3:
                top3_comp = top3_comp[:3]
            if len(top3_match) > 3:
                top3_match = top3_match[:3]
            # get number of matches
            n = 100
            for mass in top3_comp:
                #mass = top3_comp[2] #testing
                if any([True for t in top3_match if t > mass-0.0012 and t < mass+0.0012]):
                    n = n/2
                    
            sim_matrix.iloc[comp_id, match_id] = n
            
    return(sim_matrix)







