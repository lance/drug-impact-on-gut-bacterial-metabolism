#!/bin/bash


while read p; do
    curl -d "identifiers=${p}&format=full&ncbiTaxonId=small molecule" stitch.embl.de/api/tsv/resolveList | grep 'small molecule' | sed "s/^/${p}\t/"
done <../drugs.txt


