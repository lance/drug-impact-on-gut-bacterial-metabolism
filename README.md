# Drug impact on gut bacterial metabolism

In this project, we investigated drug impact on gut bacteria by analyzing a large untergeted metabolomics data set and integrating drug-protein interaction networks from STITCH.